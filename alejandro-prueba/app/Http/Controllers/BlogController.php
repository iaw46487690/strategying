<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Blog;
use App\Models\Type;
use Illuminate\Support\Facades\DB;
use App\Services\UploadPostPicService;
use Illuminate\Support\Facades\Redirect;

class BlogController extends Controller
{
    /*public $blog;
    public $type;

    public function __construct() {
        $this->blog = new Blog;
        $this->type = new Type;
    }*/

    public function category($id)
    {
        $category = Blog::where('category_id', '=', $id)->get();
        $type = Type::where('id', '=', $id)->get();
        return view('category')
            ->with('category', $category)
            ->with('type', $type);
    }

    public function getAdmin() {
        return view('admin');
    }

    public function getAdminAdd() {
        return view('add');
    }

    public function getAdminRemove() {
        $post = Blog::category()->get();
        return view('remove')
            ->with('post', $post);
    }

    public function getUpdateList() {
        $post = Blog::category()->get();
        return view('updateList')
            ->with('post', $post);
    }

    public function updatePost($id) {
        $post = Blog::where('id', '=', $id)->get();
        return view('update')
            ->with('post', $post);
    }

    public function update(Request $request, UploadPostPicService $UploadPostPicService, $id) {
        $info = Blog::where('id', '=', $id)->first();
        if ($request->input('category_id') != "") {
            $info->category_id = $request->input('category_id');
        }
        $info->title =  $request->input('title');
        $info->description =  $request->input('description');
        if ($request->file('img')) {
            $this->uploadService = $UploadPostPicService;
            $this->uploadService->uploadFile($request->file('img'));
            $newImg = $request->img->getClientOriginalName();
            $info->img = $newImg;
        }
        $info->save();
        return $this->getUpdateList();
    }

    public function search(Request $request, $action) {
        if ($request->filled('search')) {
            $search = $request->input('search');
            $post = Blog::category()->where('title', 'LIKE', '%' . $search . '%')->get();
        } else {
            $post = Blog::category()->get();
        }

        if ($action == "update") {
            return view('updateList')
            ->with('post', $post);
        }

        return view('remove')
            ->with('post', $post);

    }

    public function add(Request $request, UploadPostPicService $UploadPostPicService) {
        $newPost = new Blog;
        $newPost->category_id = $request->input('category_id');
        $newPost->title = $request->input('title');
        $newPost->description = $request->input('description');
        $this->uploadService = $UploadPostPicService;
        $this->uploadService->uploadFile($request->file('img'));
        $postImg = $request->img->getClientOriginalName();
        $newPost->img = $postImg;
        $newPost->save();
        return view('add');
    }

    public function remove($id) {
        $post = Blog::where('id', '=', $id)->first();
        $post->delete();
        return  Redirect::to(url()->previous());
    }
    /*
    public function news()
    {
        $news = $this->blog->where()->get();
        return view('news')->with('news', $news);
    }

    public function sport()
    {
        $sport = $this->blog->get();
        return view('sport')->with('sport', $sport);
    }

    public function science()
    {
        $science = $this->blog->get();
        return view('science')->with('science', $science);
    }

    public function music()
    {
        $music = $this->blog->get();
        return view('music')->with('music', $music);
    }
    */
}
