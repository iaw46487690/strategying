<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Facades\Route;
use App\Models\Blog;
use Livewire\WithFileUploads;

class Add extends Component
{
    use WithFileUploads;
    public $blog;
    public $img;

    protected $rules = [
        'blog.category_id' => 'required',
        'blog.title' => 'required|min:6',
        'blog.description' => 'required|min:10',
        'img' => '',
    ];

    public function mount(Blog $blog)
    {
        /*if ($blog) {
            $this->blog = $blog;
        } else {
            $this->blog = new Blog();
        }*/
        if (Route::currentRouteName() == "update") {
            $id = Route::current()->parameter('id');
            //$post = Blog::find($id);
            $this->blog = Blog::find($id);
        } else {
            $this->blog = new Blog();
        }
    }

    public function add()
    {
        $this->validate();
        if ($this->img != null) {
            $this->img->storeAs('img/',$this->img->getClientOriginalName(),'public');
            $this->blog->img = $this->img->getClientOriginalName();
        }
        $this->blog->save();
        $action = array('action' => "add", 'message' => "Added post " . $this->blog->title . " successfully!");
        session()->put('success', $action);
        return redirect()->route('list');
    }

    public function update()
    {
        $this->validate();
        if ($this->img != null) {
            $this->img->storeAs('img/',$this->img->getClientOriginalName(),'public');
            $this->blog->img = $this->img->getClientOriginalName();
        }
        $this->blog->save();
        $action = array('action' => "update", 'message' => "Updated post " . $this->blog->title . " successfully!");
        session()->put('success', $action);
        //return redirect('/live-index');
        return redirect()->route('list');
    }

    public function render()
    {
        return view('livewire.add')
            ->layout('layouts.app2');
    }
}
