<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Blog;

class ListBlog extends Component
{
    public $searchPost;
    public $post;

    public function mount() {
        $this->post = Blog::category()->get();
    }

    public function search() {
        $this->post = Blog::category()->where('title', 'LIKE', '%' . $this->searchPost . '%')->get();
    }

    public function remove($id) {
        $blog = Blog::find($id);
        $action = array('action' => "remove", 'message' => "Deleted post " . $blog->title . " successfully!");
        session()->put('success', $action);
        $blog->delete();
        return redirect()->route('list');
    }

    public function render()
    {
        return view('livewire.list-blog')
            //->with('post', $this->post)
            ->layout('layouts.app2');
    }
}
