<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Contact;

class Prueba extends Component
{
    public $name;
    public $email;
    public $route = "prueba";
    public $count = 0;

    public function increment()
    {
        $this->count++;
    }

    protected $rules = [
        'name' => 'required|min:6',
        'email' => 'required|email|exists:contacts',
    ];

    public function submit()
    {
        $this->validate();

        // Execution doesn't reach here if validation fails.

        /*Contact::create([
            'name' => $this->name,
            'email' => $this->email,
        ]);*/

        $info = Contact::where('email', '=', $this->email)->first();
        $info->name = $this->name;
        $info->save();
        $this->route = "add";
    }

    public function render()
    {
        return view('livewire.' . $this->route);
    }
}
