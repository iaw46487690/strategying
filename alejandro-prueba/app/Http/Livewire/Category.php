<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Blog;
use App\Models\Type;

class Category extends Component
{
    public $category;
    public $type;

    public function mount($id) {
        $this->category = Blog::where('category_id', '=', $id)->get();
        $this->type = Type::find($id);
    }

    public function render()
    {
        return view('livewire.category');
    }
}
