<?php

namespace App\Services;

class UploadPostPicService
{

    function uploadFile($file)
    {
        $destinationPath = 'img/';
        $originalFile = $file->getClientOriginalName();
        $file->move($destinationPath, $originalFile);
    }
}
