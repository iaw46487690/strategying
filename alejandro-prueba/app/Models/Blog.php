<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use HasFactory;

    protected $fillable = [
        'category_id',
        'title',
        'description',
        'img',
    ];

    public function scopeCategory($query) {
        return $query->select('blogs.*', 'types.category')->join('types', 'types.id', '=', 'blogs.category_id');
    }

}
