<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta charset="UTF-8">
    <title>Blog Alejandro</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css">
</head>
<body>
    <div class="admin">
        <a href="/">
            <span class="fa fa-user-times">&nbsp;&nbsp;ADMIN</span>
        </a>
    </div>
    <div class="menu">
        <ul class="nav2">
            <li>
                <a href="/admin/add">
                    <span class="fa fa-plus-circle">&nbsp;&nbsp;Add a post</span>
                </a>
            </li>
            <li>
                <a href="/admin/update">
                    <span class="fa fa-minus-circle">&nbsp;&nbsp;Update a post</span>
                </a>
            </li>
            <li>
                <a href="/admin/remove">
                    <span class="fa fa-minus-circle">&nbsp;&nbsp;Remove a post</span>
                </a>
            </li>
        </ul>
    </div>
    {{-- Because she competes with no one, no one can compete with her. --}}
    <div class="create">
        <h2 class="center">UPDATE A POST</h2>
        <div class="divide"></div>
        <form action="/update/{{ $post[0]->id }}" method="POST"  enctype="multipart/form-data">
            @csrf
            <h4>Category:</h4>
            <select id="category_id" name="category_id">
                <option value="" selected>---</option>
                <option value="1">NEWS</option>
                <option value="2">SPORT</option>
                <option value="3">SCIENCE</option>
                <option value="4">MUSIC</option>
            </select>
            <h4>Title:</h4>
            <input  type="text" name="title" wire:model="title" class="input" value="{{ $post[0]->title }}" placeholder="Title" required/>
            <h4>Description:</h4>
            <textarea id="description" name="description" wire:model="description" class="input" style="height: 200px" placeholder="Description" required>{{ $post[0]->description }}</textarea>
            <h4>Imagen:</h4>
            <input type="file" name="img" id="img">
            <br><br><br>
            <button type="submit" class="btn" style="background: #ffdf13"><span>Update post</span></button>
        </form>
    </div>
</body>
</html>