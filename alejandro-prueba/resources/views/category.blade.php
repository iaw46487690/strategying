<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta charset="UTF-8">
    <title>Blog Alejandro</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css">
</head>
<body>
    <div class="admin">
        <a href="/admin">
            <span class="fa fa-user-plus">&nbsp;&nbsp;ADMIN</span>
        </a>
    </div>
    <div class="menu">
        <ul class="nav">
            <li>
                <a href="/">
                    <span class="fa fa-home">&nbsp;&nbsp;Inicio</span>
                </a>
            </li>
            <li>
                <a href="/category/1">
                    <span class="fa fa-newspaper-o">&nbsp;&nbsp;News</span>
                </a>
            </li>
            <li>
                <a href="/category/2">
                    <span class="fa fa-futbol-o">&nbsp;&nbsp;Sport</span>
                </a>
            </li>
            <li>
                <a href="/category/3">
                    <span class="fa fa-flask">&nbsp;&nbsp;Science</span>
                </a>
            </li>
            <li>
                <a href="/category/4">
                    <span class="fa fa-spotify">&nbsp;&nbsp;Music</span>
                </a>
            </li>
        </ul>
    </div>
    <div class="title">
        {{ $type[0]->category }}
    </div>
    @forelse ($category as $item)
        <div class="content center">
            <h3>{{ $item->title }}</h3>
            <p class="middle">
                {{ $item->description }}
            </p>
            <img class="img" src="{{asset('img/').'/'.$item->img}}" width="50%">
        </div>
        <div class="divide"></div>
    @empty
        <div class="center">No hay ningún post para esta categoria</div>
    @endforelse
    <div class="footer">Footer</div>
</body>
</html>