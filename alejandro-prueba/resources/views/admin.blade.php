<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta charset="UTF-8">
    <title>Blog Alejandro</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css">
</head>
<body>
    <div class="admin">
        <a href="/">
            <span class="fa fa-user-times">&nbsp;&nbsp;ADMIN</span>
        </a>
    </div>
    <div class="menu">
        <ul class="nav2">
            <li>
                <a href="/admin/add">
                    <span class="fa fa-plus-circle">&nbsp;&nbsp;Add a post</span>
                </a>
            </li>
            <li>
                <a href="/admin/update">
                    <span class="fa fa-minus-circle">&nbsp;&nbsp;Update a post</span>
                </a>
            </li>
            <li>
                <a href="/admin/remove">
                    <span class="fa fa-minus-circle">&nbsp;&nbsp;Remove a post</span>
                </a>
            </li>
        </ul>
    </div>
    <div class="fotoAdmin">
        <img src="img/admin.jpg" width="100%" max-heigth="100%">
    </div>
</body>
</html>