<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta charset="UTF-8">
    <title>Blog Alejandro</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css">
</head>
<body>
    <div class="admin">
        <a href="/">
            <span class="fa fa-user-times">&nbsp;&nbsp;ADMIN</span>
        </a>
    </div>
    <div class="menu">
        <ul class="nav2">
            <li>
                <a href="/admin/add">
                    <span class="fa fa-plus-circle">&nbsp;&nbsp;Add a post</span>
                </a>
            </li>
            <li>
                <a href="/admin/update">
                    <span class="fa fa-minus-circle">&nbsp;&nbsp;Update a post</span>
                </a>
            </li>
            <li>
                <a href="/admin/remove">
                    <span class="fa fa-minus-circle">&nbsp;&nbsp;Remove a post</span>
                </a>
            </li>
        </ul>
    </div>
    <div class="create">
        <h2 class="center">REMOVE A POST</h2>
            <div class="divide"></div>
        <form class="search" action="/search/remove" method="post">
            @csrf
            <button class="searchBtn"><a href="/admin/remove"></a>Show all</button>
            <input type="text" name="search" class="searchInput" placeholder="Search by title . . .">
            <button type="submit" class="searchBtn"><span class="fa fa-search"></span></button>
        </form>
        @forelse ($post as $item)
            <div class="remove">
                Category: {{ $item->category }}
                <h3 class="center">{{ $item->title }}</h3>
                <form action="/remove/{{ $item->id }}" method="POST">
                    @csrf 
                    <button type="submit" class="rmButton"><span>REMOVE POST</span></button>
                </form>
            </div>
        @empty
            <div class="center">No se ha encontrado ningún post</div>
        @endforelse
    </div>
</body>
</html>