<div class="create">
    @if ($blog->id == null)
        <h2 class="center">ADD NEW POST</h2>
    @else
        <h2 class="center">UPDATE POST</h2>
    @endif
    <div class="divide"></div>
    {{-- Do your work, then step back. --}}
    @csrf
    <h4>Category:</h4>
    @error('blog.category_id') <span class="error">{{ $message }}</span><br> @enderror
    <select wire:model="blog.category_id" id="category_id" name="category_id" required>
        <option selected value={{null}}>---</option>
        <option value="1">NEWS</option>
        <option value="2">SPORT</option>
        <option value="3">SCIENCE</option>
        <option value="4">MUSIC</option>
    </select>
    <h4>Title:</h4>
    @error('blog.title') <span class="error">{{ $message }}</span><br> @enderror
    <input wire:model="blog.title" type="text" name="title" class="input" placeholder="Title">
    <h4>Description:</h4>
    @error('blog.description') <span class="error">{{ $message }}</span><br> @enderror
    <textarea wire:model="blog.description" id="description" name="description" class="input" style="height: 200px" placeholder="Description"></textarea>
    <h4>Image:</h4>
    @error('img') <span class="error">{{ $message }}</span><br> @enderror
    <input wire:model="img" type="file" name="img" id="img">
    <br><br><br>
    @if ($blog->id == null)
        <button wire:click="add" type="button" class="btn"><span>Add new post</span></button>
    @else
        <button wire:click="update({{$blog->id}})" type="button" class="btn" style="background-color: #ffdf13"><span>Update post</span></button>
    @endif
    {{--
    <h2 class="center">UPDATE POST</h2>
    <button wire:click="update({{$blog->id}})" type="button" class="btn" style="background-color: #ffdf13"><span>Update post</span></button>
    --}}
</div>
