<div>
    {{-- Do your work, then step back. --}}
    <div class="title">
        {{ $type->category }}
    </div>
    @forelse ($category as $item)
        <div class="content center">
            <h3>{{ $item->title }}</h3>
            <p class="middle">
                {{ $item->description }}
            </p>
            <img class="img" src="{{asset('img/').'/'.$item->img}}" width="50%">
        </div>
        <div class="divide"></div>
    @empty
        <br><br>
        <div class="center" style="font-size: 20px">No hay ningún post para esta categoria</div>
    @endforelse
</div>
