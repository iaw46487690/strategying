<div>
    <div>
        {{-- If you look to others for fulfillment, you will never truly be fulfilled. --}}
        <h1>Prueba livewire</h1>
        {{$name}}
    </div>
    <div style="text-align: center">
        <button wire:click="increment">+</button>
        <h1>{{ $count }}</h1>
    </div>
    <div>
        Name: <input type="text" wire:model="name" />
        <br><br>
        event -> wire:model: {{$name}}
    </div>
    <br><br>
    New Contact:
    <form wire:submit.prevent="submit">
        Name: <input type="text" wire:model="name">
        @error('name') <span class="error">{{ $message }}</span> @enderror
        <br>
        Email: <input type="text" wire:model="email">
        @error('email') <span class="error">{{ $message }}</span> @enderror
        <br>
        <button type="submit">Save Contact</button>
    </form>
</div>
