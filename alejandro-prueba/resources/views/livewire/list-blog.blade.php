<div class="create">
    <h2 class="center">UPDATE OR REMOVE A POST</h2>
    <div class="divide"></div><br>
    @if (session()->has('success'))
        @if (session()->get('success')['action'] == 'add')
            <div class="alert-green">
                {{session()->get('success')['message']}}
            </div><br>
        @elseif ((session()->get('success')['action'] == 'remove'))
            <div class="alert">
                {{session()->get('success')['message']}}
            </div><br>
        @else
            <div class="alert-yellow">
                {{session()->get('success')['message']}}
            </div><br>
        @endif
        @php session()->forget('success') @endphp
    @endif
    {{-- Knowing others is intelligence; knowing yourself is true wisdom. --}}
    @csrf
    <button class="searchBtn2"><a href="{{ route('list') }}" class="link">SHOW ALL</a></button>
    <input wire:model.defer="searchPost" type="text" name="search" class="searchInput" placeholder="Search by title . . .">
    <button wire:click="search" type="button" class="searchBtn"><span class="fa fa-search"></span></button>
    @forelse ($post as $item)
        <div class="remove">
            Category: {{ $item->category }}
            <h3 class="center">{{ $item->title }}</h3>
            @csrf
            <button wire:click="remove({{$item->id}})" type="button" class="rmButton"><span>REMOVE POST</span></button>
            <button class="upButton2"><a href="{{ route('update', ['id' => $item->id]) }}" class="link"><span>UPDATE POST</span></a></button>
        </div>
    @empty
        <br><br><br>
        <div class="center">No se ha encontrado ningún post</div>
    @endforelse
</div>
