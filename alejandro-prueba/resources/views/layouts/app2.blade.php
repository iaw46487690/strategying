<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta charset="UTF-8">
    <title>Blog Alejandro</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css">
    @livewireStyles
</head>
<body>
    <div class="admin-top">
        <a href="/live-blog" class="link">
            <span class="fa fa-newspaper-o">&nbsp;&nbsp;BLOG</span>
        </a>
    </div>
    <div class="menu">
        <ul class="nav3">
            <li>
                <a href="/live-add" class="link">
                    <span class="fa fa-plus-circle">&nbsp;&nbsp;ADD POST</span>
                </a>
            </li>
            <li>
                <a href="/live-list" class="link">
                    <span class="fa fa-minus-circle">&nbsp;&nbsp;UPDATE / REMOVE POST</span>
                </a>
            </li>
        </ul>
    </div>
    {{ $slot }}
    @livewireScripts
</body>
</html>
