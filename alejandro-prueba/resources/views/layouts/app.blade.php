<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta charset="UTF-8">
    <title>Blog Alejandro</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css">
    @livewireStyles
</head>
<body>
    <div class="admin">
        <a href="/live-list" class="link">
            <span class="fa fa-user-plus">&nbsp;&nbsp;ADMIN</span>
        </a>
    </div>
    <div class="menu">
        <ul class="nav">
            <li>
                <a href="/live-blog" class="link">
                    <span class="fa fa-home">&nbsp;&nbsp;INICIO</span>
                </a>
            </li>
            <li>
                <a href="/live-category/1" class="link">
                    <span class="fa fa-newspaper-o">&nbsp;&nbsp;NEWS</span>
                </a>
            </li>
            <li>
                <a href="/live-category/2" class="link">
                    <span class="fa fa-futbol-o">&nbsp;&nbsp;SPORT</span>
                </a>
            </li>
            <li>
                <a href="/live-category/3" class="link">
                    <span class="fa fa-flask">&nbsp;&nbsp;SCIENCE</span>
                </a>
            </li>
            <li>
                <a href="/live-category/4" class="link">
                    <span class="fa fa-spotify">&nbsp;&nbsp;MUSIC</span>
                </a>
            </li>
        </ul>
    </div>
    {{ $slot }}
    @livewireScripts
</body>
</html>
