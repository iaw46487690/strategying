<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('types')->insert([
            'category' => 'NEWS'
        ]);

        DB::table('types')->insert([
            'category' => 'SPORT'
        ]);

        DB::table('types')->insert([
            'category' => 'SCIENCE'
        ]);

        DB::table('types')->insert([
            'category' => 'MUSIC'
        ]);
    }
}
