<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contacts')->insert([
            'name' => 'Alejandro',
            'email' => 'alejandro@gmail.com'
        ]);

        DB::table('contacts')->insert([
            'name' => 'Ruben',
            'email' => 'ruben@gmail.com'
        ]);
    }
}
