<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('blogs')->insert([
            'category_id' => 1,
            'title' => 'News 1',
            'description' => 'El presidente del Gobierno, Pedro Sánchez, ha anunciado este lunes que España exigirá una PCR negativa o vacuna completa a todos los ciudadanos que vengan del Reino Unido a nuestro país. La medida comenzará a aplicarse dentro de 72 horas.
            Sánchez ha justificado la medida en el hecho de que los datos que llegan del Reino Unido son preocupantes porque tienen unos contagios "muy por encima de los 150 casos por 100.000 habitantes en 14 días". Por eso, ha dicho Sánchez, España tenía que tomar "alguna precaución adicional respecto a la llegada de turistas británicos".',
            'img' => 'news.jfif'
        ]);

        DB::table('blogs')->insert([
            'category_id' => 1,
            'title' => 'News 2',
            'description' => 'El presidente del Gobierno, Pedro Sánchez, ha anunciado este lunes que España exigirá una PCR negativa o vacuna completa a todos los ciudadanos que vengan del Reino Unido a nuestro país. La medida comenzará a aplicarse dentro de 72 horas.
            Sánchez ha justificado la medida en el hecho de que los datos que llegan del Reino Unido son preocupantes porque tienen unos contagios "muy por encima de los 150 casos por 100.000 habitantes en 14 días". Por eso, ha dicho Sánchez, España tenía que tomar "alguna precaución adicional respecto a la llegada de turistas británicos".',
            'img' => 'news2.png'
        ]);

        DB::table('blogs')->insert([
            'category_id' => 2,
            'title' => 'Sport 1',
            'description' => 'El presidente del Gobierno, Pedro Sánchez, ha anunciado este lunes que España exigirá una PCR negativa o vacuna completa a todos los ciudadanos que vengan del Reino Unido a nuestro país. La medida comenzará a aplicarse dentro de 72 horas.
            Sánchez ha justificado la medida en el hecho de que los datos que llegan del Reino Unido son preocupantes porque tienen unos contagios "muy por encima de los 150 casos por 100.000 habitantes en 14 días". Por eso, ha dicho Sánchez, España tenía que tomar "alguna precaución adicional respecto a la llegada de turistas británicos".',
            'img' => 'sport1.jpg'
        ]);
        
        DB::table('blogs')->insert([
            'category_id' => 2,
            'title' => 'Sport 2',
            'description' => 'El presidente del Gobierno, Pedro Sánchez, ha anunciado este lunes que España exigirá una PCR negativa o vacuna completa a todos los ciudadanos que vengan del Reino Unido a nuestro país. La medida comenzará a aplicarse dentro de 72 horas.
            Sánchez ha justificado la medida en el hecho de que los datos que llegan del Reino Unido son preocupantes porque tienen unos contagios "muy por encima de los 150 casos por 100.000 habitantes en 14 días". Por eso, ha dicho Sánchez, España tenía que tomar "alguna precaución adicional respecto a la llegada de turistas británicos".',
            'img' => 'sport2.png'
        ]);

        DB::table('blogs')->insert([
            'category_id' => 3,
            'title' => 'Science 1',
            'description' => 'El presidente del Gobierno, Pedro Sánchez, ha anunciado este lunes que España exigirá una PCR negativa o vacuna completa a todos los ciudadanos que vengan del Reino Unido a nuestro país. La medida comenzará a aplicarse dentro de 72 horas.
            Sánchez ha justificado la medida en el hecho de que los datos que llegan del Reino Unido son preocupantes porque tienen unos contagios "muy por encima de los 150 casos por 100.000 habitantes en 14 días". Por eso, ha dicho Sánchez, España tenía que tomar "alguna precaución adicional respecto a la llegada de turistas británicos".',
            'img' => 'science1.png'
        ]);
        
        DB::table('blogs')->insert([
            'category_id' => 3,
            'title' => 'Science 2',
            'description' => 'El presidente del Gobierno, Pedro Sánchez, ha anunciado este lunes que España exigirá una PCR negativa o vacuna completa a todos los ciudadanos que vengan del Reino Unido a nuestro país. La medida comenzará a aplicarse dentro de 72 horas.
            Sánchez ha justificado la medida en el hecho de que los datos que llegan del Reino Unido son preocupantes porque tienen unos contagios "muy por encima de los 150 casos por 100.000 habitantes en 14 días". Por eso, ha dicho Sánchez, España tenía que tomar "alguna precaución adicional respecto a la llegada de turistas británicos".',
            'img' => 'science2.png'
        ]);

        DB::table('blogs')->insert([
            'category_id' => 4,
            'title' => 'Music 1',
            'description' => 'El presidente del Gobierno, Pedro Sánchez, ha anunciado este lunes que España exigirá una PCR negativa o vacuna completa a todos los ciudadanos que vengan del Reino Unido a nuestro país. La medida comenzará a aplicarse dentro de 72 horas.
            Sánchez ha justificado la medida en el hecho de que los datos que llegan del Reino Unido son preocupantes porque tienen unos contagios "muy por encima de los 150 casos por 100.000 habitantes en 14 días". Por eso, ha dicho Sánchez, España tenía que tomar "alguna precaución adicional respecto a la llegada de turistas británicos".',
            'img' => 'music1.jpg'
        ]);
        
        DB::table('blogs')->insert([
            'category_id' => 4,
            'title' => 'Music 2',
            'description' => 'El presidente del Gobierno, Pedro Sánchez, ha anunciado este lunes que España exigirá una PCR negativa o vacuna completa a todos los ciudadanos que vengan del Reino Unido a nuestro país. La medida comenzará a aplicarse dentro de 72 horas.
            Sánchez ha justificado la medida en el hecho de que los datos que llegan del Reino Unido son preocupantes porque tienen unos contagios "muy por encima de los 150 casos por 100.000 habitantes en 14 días". Por eso, ha dicho Sánchez, España tenía que tomar "alguna precaución adicional respecto a la llegada de turistas británicos".',
            'img' => 'music2.jfif'
        ]);
    }
}
