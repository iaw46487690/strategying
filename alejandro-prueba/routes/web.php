<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BlogController;
use App\Http\Livewire\Prueba;
use App\Http\Livewire\Add;
use App\Http\Livewire\ListBlog;
use App\Http\Livewire\Blog;
use App\Http\Livewire\Category;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});
*/

Route::get('/', function () {
    return view('blog');
});

/*
Route::get('/news', [blog_controller::class, "news"]);
Route::get('/sport', [blog_controller::class, "sport"]);
Route::get('/science', [blog_controller::class, "science"]);
Route::get('/music', [blog_controller::class, "music"]);
*/

Route::get('/category/{id}', [BlogController::class, "category"]);
Route::get('/admin', [BlogController::class, "getAdmin"]);
Route::get('/admin/add', [BlogController::class, "getAdminAdd"]);
Route::post('/add', [BlogController::class, "add"]);
Route::get('/admin/remove', [BlogController::class, "getAdminRemove"]);
Route::post('/remove/{id}', [BlogController::class, "remove"]);
Route::get('/admin/update', [BlogController::class, "getUpdateList"]);
Route::post('/updatePost/{id}', [BlogController::class, "updatePost"]);
Route::post('/update/{id}', [BlogController::class, "update"]);
Route::post('/search/{action}', [BlogController::class, "search"]);
Route::get('/search', [BlogController::class, "search"]);

// livewire
Route::get('/live', Prueba::class);
// layout app
Route::get('/live-blog', Blog::class)->name('blog');
Route::get('/live-category/{id}', Category::class)->name('category');
//layout app2
Route::get('/live-add', Add::class)->name('add');
Route::get('/live-update/{id}', Add::class)->name('update');
Route::get('/live-list', ListBlog::class)->name('list');
